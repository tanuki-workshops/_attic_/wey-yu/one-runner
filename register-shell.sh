set -o allexport; source .env; set +o allexport

sudo gitlab-runner register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_TOKEN_WEYYU_RUNNERS}" \
  --executor "shell" \
  --description "${RUNNER_DESCRIPTION}" \
  --tag-list "shell,gitpod" \
  --run-untagged="true" \
  --locked="false"

